extends RigidBody2D

signal collected
onready var MainCharacter = get_parent().get_node('MainCharacter')

func _ready():
    pass
    
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
    var bodies = get_colliding_bodies()
    for body in bodies: 
        if body.name == 'MainCharacter': 
            emit_signal('collected')
            MainCharacter.equip_item(self)
            queue_free()
        