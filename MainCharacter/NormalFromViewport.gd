extends Node2D

enum Facing { RIGHT, LEFT }
export var NORM_MSAA: bool
export var NORM_HDR: bool
export var NORM_KEEP_3D_LINEAR: bool
export var NORM_UPDATE_ALWAYS: bool
export var NORM_SHOW_DEBUG: bool
export var DEBUG_SKELETON_2D: bool

onready var CharacterSprite: Sprite = $CharacterSprite
onready var CharacterViewport: Viewport = $CharacterViewport
onready var CharacterArmature = CharacterViewport.get_node("Scene Root/Armature")
onready var CharacterAnimationPlayer: AnimationPlayer = CharacterViewport.get_node('Scene Root/AnimationPlayer')
onready var CharacterCamera: Camera = CharacterViewport.get_node("Scene Root/Armature/HipsAttachment/Camera")

onready var Norm_Sprite: Sprite  = $Norm_Sprite
onready var Norm_Viewport: Viewport = CharacterViewport.duplicate(DUPLICATE_USE_INSTANCING)
onready var Norm_Armature: Skeleton = Norm_Viewport.get_node("Scene Root/Armature")
onready var Norm_AnimationPlayer: AnimationPlayer = Norm_Viewport.get_node('Scene Root/AnimationPlayer')
onready var Norm_Camera: Camera = Norm_Viewport.get_node("Scene Root/Armature/HipsAttachment/Camera")
onready var Norm_Material_Resource: Material = load("res://MainCharacter/Norm_Material.tres")
onready var Norm_MeshInstance_Body: MeshInstance = Norm_Viewport.get_node("Scene Root/Armature/Kachujin")
onready var Bone_Count: int
onready var Occluder: LightOccluder2D = $CharacterSprite/LightOccluder2D
var Bone_Positions: Array
var is_facing_right: bool = true
var is_facing_left: bool = false

func _ready():
    add_child(Norm_Viewport)
    Norm_Armature.translation.z += 1000
    CharacterSprite.normal_map = Norm_Viewport.get_texture()
    Bone_Count = CharacterArmature.get_bone_count()
    
    for mesh in [Norm_MeshInstance_Body]:
        mesh.material_override = Norm_Material_Resource

func facing(facing):
    var camera_rotation_y = CharacterCamera.rotation_degrees.y
    is_facing_right = facing == Facing.RIGHT
    is_facing_left = facing == Facing.LEFT
    var character_rotation_y = 90 * (1 if is_facing_right else -1)
    
    if (camera_rotation_y != character_rotation_y):
        CharacterCamera.transform.origin.x *= -1
        CharacterCamera.rotation_degrees.y = character_rotation_y
        Norm_Camera.transform = CharacterCamera.transform
        
func _draw():
    if(DEBUG_SKELETON_2D):
        draw_polyline(Bone_Positions, Color.coral, 1)
        
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
    if NORM_UPDATE_ALWAYS: Norm_Viewport.render_target_update_mode = Viewport.UPDATE_ALWAYS
    if NORM_MSAA: Norm_Viewport.msaa = Viewport.MSAA_16X
    if NORM_HDR: Norm_Viewport.hdr = false
    if NORM_KEEP_3D_LINEAR: Norm_Viewport.keep_3d_linear = true
    if NORM_SHOW_DEBUG: Norm_Sprite.texture = Norm_Viewport.get_texture()
    
    if Norm_AnimationPlayer.current_animation != CharacterAnimationPlayer.current_animation:
        Norm_AnimationPlayer.current_animation = CharacterAnimationPlayer.current_animation
    
    Bone_Positions.clear()
    for bone_index in range(Bone_Count):
        var bone_transform: Transform = CharacterArmature.get_bone_global_pose(bone_index)
        
        if bone_transform:
            var bone_position:Vector2 = CharacterCamera.unproject_position(bone_transform.origin)
            bone_position = CharacterSprite.to_local(bone_position+get_parent().position) / 10
            bone_position.y += 25
            bone_position.x -= 2
            Bone_Positions.insert(bone_index, bone_position)
    
    Occluder.occluder.polygon = PoolVector2Array(Bone_Positions)
    if DEBUG_SKELETON_2D: update()
