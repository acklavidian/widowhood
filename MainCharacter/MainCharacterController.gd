extends KinematicBody2D

export (int) var run_speed = 1
export (int) var jump_speed = -400
export (int) var gravity = 1200

var velocity = Vector2()
var jumping = false
var attack_count: int = 0
var action_queue: Array =  []
var animation_name: String = 'Idle'

onready var CharacterAnimationPlayer: AnimationPlayer = get_node("Character/CharacterViewport/Scene Root/AnimationPlayer")
onready var CharacterSprite: Sprite = get_node("Character/CharacterSprite")
onready var Character: Node2D = get_node('Character')
onready var RightHandAttachment: BoneAttachment = get_node('Character/CharacterViewport/Scene Root/Armature/RightHandAttachment')
onready var Text: Label = get_node("Label")
    
func equip_item(item):
    var model: Spatial = load('res://FirstSword/3DFirstSword.tscn').instance()
    RightHandAttachment.add_child(model)
    print(model.translation)
    
func get_input():
    var right = Input.is_action_pressed('move right')
    var left = Input.is_action_pressed('move left')
    var jump = Input.is_action_just_pressed('jump')
    var attack = Input.is_action_just_pressed('attack')

    if attack:
        attack()
    elif action_queue.size() == 0: 
        if jump and is_on_floor():
            velocity.y = jump_speed
        if right:
            velocity.x = run_speed
        if left:
            velocity.x = -run_speed
        if !(left || right || jump):
            Text.text = String(velocity.x)
            velocity.x = velocity.x / 2

func _physics_process(delta):
    get_input()
    
    if velocity.x >= 1:
        Character.facing(Character.Facing.LEFT)
    elif velocity.x <= -1: 
        Character.facing(Character.Facing.RIGHT)
    
#	velocity.x =  ease(velocity.x, 111111)
    velocity.y += gravity * delta
    velocity = move_and_slide(velocity, Vector2(0, -1))
    pick_animation()
#	Text.text = String(velocity.x)
    

func pick_animation():
    if action_queue.size() > 0:
        CharacterAnimationPlayer.play(action_queue.back())
        dash(100)
        yield(CharacterAnimationPlayer, "animation_finished")
        action_queue.pop_back()
    elif (!is_on_floor()):
        CharacterAnimationPlayer.play('Jump')
    elif abs(velocity.x) >= 1:
        CharacterAnimationPlayer.play('Sprint')
    else:
        CharacterAnimationPlayer.play('Idle')

func dash(amount):
    var dir_multiplier = -1 if Character.is_facing_right else 1
    velocity.x = amount * dir_multiplier
    
    
func attack():
    
    print(action_queue)
    match action_queue:
        []: 
            action_queue.push_back('AttackOne')
        ['AttackOne']:
            action_queue.push_back('AttackTwo')
        ['AttackOne', 'AttackTwo'], ['AttackTwo']:
            action_queue.push_back('AttackThree')
