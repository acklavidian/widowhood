# Widowhood

Its a 2D sidescroller.

# Premise

 Widowhood is based around a the love and relationships of a reclusive siren. Despite what legends suggest the siren isn't an evil entity. Atleast not anymore, as she has lived so long that no one, not even herself can recall the beginning. However, she can recall from her earliest memories being very alone. Initially, the siren couldn't reconcile the idea of a relationship given her immortality, so she isolated herself. However, in the face of eternal solitude she allows herself to become close to others despite knowing that she will always out live them.
(read: better to have loved and lost than to have never loved at all yadayada)

(I don't know if it will be in a prelude or if I will let the player uncover this as they progress...)

 Despite her immortality she lives as normally as she can away from society. This includes falling in love just as organically as anyone else. She has come to accept that she will always survive her loved ones. She finds a solace and her purpose in caring for her significant others as they live out their life. She will be characterized as mostly falling for the disenfranchised/wayward/unfortunate often veterans or adventurers who have lived beyond their glory days. She is an overwhelmingly positive influence to these people.  She will be portrayed as having always existed and she uses her eternal wisdom to bring out the best of them and making them happy before they move on. (read: scrooge kinda? groundhogs day but with relationships?)...

She doesn't hide her romantic history nor her immortality from her companions. Scattered through out the woods nearby her house are the shrines and graves of all those she has loved and lost. She is the embodiment of "good".


A ruler of a near by village has caught wind of the legend of the siren of Widowhood. He finds out about the passing of her lover and takes the opportunity to try an sire and everlasting evil heir to his throne. (He has a whole back story but I'll spare the deets for now. Just know that he is evil and studys black magic -specifically conjuration- in an effort to "create" and heir. Anyhow, he has hordes of incompetent failed experiments. Read: evil zombie horde).

The game opens up as the siren mourns the loss of her most recent lover. The King invades the home of the siren with the intention to abduct and take her back to his castle against her will. The player takes control of the siren and attempts to escape the king. This is a no win situation. But, how long you last determines the difficulty of the game. Longer = Harder. No pun intended. (Perhaps the hardest mode is playing the whole way through as the siren)

This difficulty selection won't be made immediately apparent to the player until after the event happens.
Besides a quick "We can do this the hard way or the easy way" trope

After the abduction scene ends you get to select from random graves in the "Widow's Wood".
You rise from the dead as a vengeful spirit of a past lover.
And rage through the kings evil hordes all the way to the castle to take on the king.

When you die you pick a new grave stone and burst through the ground as different vengeful spirit of the sirens past. The epitaphs are indicative of the type of stats you will get.

"hell hath no fury" = woman with fire element or something like that.

The world is left just the same as previous playthoughs allowing you to manipulate things to enable a future playthroughs and solve certain puzzles.

# Build Instructions
1. Download  [Godot 3.1](godotengine.org/download)
2. Open Project in Godot.
3. Run Project.